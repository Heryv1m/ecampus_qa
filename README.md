### QA Automation project to test "Ecampus" REST API

Use [this resource](https://httpbin.org/) as a test bench for training purposes.

See Rest-Assured [Getting Started](https://github.com/rest-assured/rest-assured/wiki/GettingStarted).

See Rest-Assured [Usage Guide](https://github.com/rest-assured/rest-assured/wiki/Usage).