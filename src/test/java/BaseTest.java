import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import model.Student;
import model.Teacher;
import org.junit.jupiter.api.Test;

import java.io.File;

import static io.restassured.RestAssured.given;

public class BaseTest {
    private static final String validUserRegistrationForStudent = "src/test/resources/validUserRegistrationAsStudent.json";
    private static final String validUserRegistrationForTeacher = "src/test/resources/validUserRegistrationAsTeacher.json";

    protected final static String authToken;
    protected final static Header authHeader;
    protected final static String authTokenForTeacher;
    protected final static Header authHeaderForTeacher;

    static {
        Response post = given()
                .baseUri("http://52.56.156.41:8080")
                .contentType(ContentType.JSON)
                .body(new File(validUserRegistrationForStudent))
                .when()
                .post(Tests.EndPoints.signin)
                .then().extract().response().prettyPeek();

        authToken = post.then().extract()
                .response().jsonPath()
                .get("accessToken").toString();

        authHeader = new Header("Authorization", "Bearer " + authToken);

        Response response = given()
                .baseUri("http://52.56.156.41:8080")
                .contentType(ContentType.JSON)
                .body(new File(validUserRegistrationForTeacher))
                .when()
                .post(Tests.EndPoints.signin)
                .then().extract().response().prettyPeek();

        authTokenForTeacher = post.then().extract()
                .response().jsonPath()
                .get("accessToken").toString();

        authHeaderForTeacher = new Header("Authorization", "Bearer " + authToken);
    }
}
