import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.internal.http.Status;
import io.restassured.response.Response;
import model.Student;
import model.Teacher;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Java6Assertions.assertThat;


public class Tests extends BaseTest {

    private static final String userRegistration = "src/test/resources/test.json";
    private static final String inValidUserRegistration = "src/test/resources/inValidUserForRegistartion.json";
    private static final String validUserRegistrationForTeacher = "src/test/resources/validUserRegistrationAsTeacher.json";
    private static final String validUserRegistrationForStudent = "src/test/resources/validUserRegistrationAsStudent.json";
    private static final String validUserForUpdate = "src/test/resources/validUserRegistrationAsStudent.json";


    public final class EndPoints {
        public static final String signin = "/api/auth/signin";
        public static final String course = "/course";
        public static final String getGroupsFromDepartment = "/api/faculty/**/department/1/group";
        public static final String profile = "/api/users/profile";
        public static final String getFaculty = "/api/faculty";
        public static final String getDepartmentsByFaculty = "/api/faculty/1/department";
        public static final String getTeachersFromDepartment = "/api/faculty/**/department/1/teacher";
        public static final String getSubjectsByTeacher = "/api/faculty/**/teacher/41/subject";
    }
    protected static Teacher teacher;
    protected static Student student;

    @BeforeAll
    static void setUp() {
        RestAssured.baseURI = "http://52.56.156.41:8080";
        RestAssured.useRelaxedHTTPSValidation();
        /*профайл и все данные для студента*/
        Response response =   given().baseUri(RestAssured.baseURI)
                .header(authHeader)
                .when()
                .get(EndPoints.profile)
                .then().extract()
                .response().prettyPeek();
        student = response.then().extract()
                .body().as(Student.class);
        /*профайл и все данные для тичера*/
        Response post =   given().baseUri(RestAssured.baseURI)
                .header(authHeaderForTeacher)
                .when()
                .get(EndPoints.profile)
                .then().extract()
                .response().prettyPeek();
        teacher = post.then().extract()
                .body().as(Teacher.class);
    }

    @Test
    public void testUpdate_Success_For_Srudent(){
        Response response = given()
                .baseUri(RestAssured.baseURI)
                .header(authHeader)
                .contentType(ContentType.JSON)
                .body(student)
                .post(EndPoints.profile)
                .then().extract().response().prettyPeek();

        assertThat(response.getStatusCode()).isEqualTo(200);
    }

    @Test
    public void testUpdate_Success_For_Teacher(){
        Response response = given()
                .baseUri(RestAssured.baseURI)
                .header(authHeaderForTeacher)
                .contentType(ContentType.JSON)
                .body(teacher)
                .post(EndPoints.profile)
                .then().extract().response().prettyPeek();

        assertThat(response.getStatusCode()).isEqualTo(200);
    }

    @Test
    void getFaculty(){
        Response post = RestAssured.given()
                .baseUri(RestAssured.baseURI)
                .when()
                .get(EndPoints.getFaculty)
                .then().extract().response()
                .prettyPeek();
        Status status = Status.find(post.getStatusCode());
        assertThat(status).isEqualTo(Status.SUCCESS);
    }

    @Test
    void getDepartmentsByFaculty(){
        Response post = RestAssured.given()
                .baseUri(RestAssured.baseURI)
                .when()
                .get(EndPoints.getDepartmentsByFaculty)
                .then().extract().response()
                .prettyPeek();
        Status status = Status.find(post.getStatusCode());
        assertThat(status).isEqualTo(Status.SUCCESS);

    }

    @Test
    void getGroupsFromDepartment(){
        Response response = given()
                .baseUri(RestAssured.baseURI)
                .when()
                .get(EndPoints.getGroupsFromDepartment);
        response.prettyPeek();
        assertThat(response.getStatusCode()).isEqualTo(200);
    }

    @Test
    void getTeachersFromDepartment(){
        Response response = given()
                .baseUri(RestAssured.baseURI)
                .when()
                .get(EndPoints.getTeachersFromDepartment);
        response.prettyPeek();
        assertThat(response.getStatusCode()).isEqualTo(200);
    }

    @Test
    void getSubjectsByTeacher(){
        Response post = given()
                .baseUri(RestAssured.baseURI)
                .when()
                .get(EndPoints.getSubjectsByTeacher)
                .then().extract().response().prettyPeek();
        assertThat(post.getStatusCode()).isEqualTo(200);
    }
//
//    @Test
//    public void testUpdate_Success_As_Srudent(){
//
//        Response response = RestAssured.given()
//                .baseUri(RestAssured.baseURI)
//           //     .header(requestSpecification)
//                .contentType(ContentType.JSON)
//                .body(validUserForUpdate)
//                .post(EndPoints.profile).then().extract().response().prettyPeek();
//
//        assertThat(response.getStatusCode()).isEqualTo(200);
//    }
//    @AfterEach
//    @Test
//    void inValidUserRegistration(){
//        Response post = RestAssured.given()
//                .baseUri(RestAssured.baseURI)
//                .log().everything()
//                .contentType(ContentType.JSON)
//                .body(new File(inValidUserRegistration))
//                .when()
//                .post("/api/auth/signup");
//
//        Status status = Status.find(post.getStatusCode());
//        assertThat(status).isEqualTo(Status.FAILURE);
//        assertThat(post.getStatusCode()).isEqualTo(400);
//    }
//    @Ignore
//    @Test
//    void testGet_Success(){
//        Response response = given()
//                .when()
//                .get("/course");
//        response.prettyPrint();
//        Status status = Status.find(response.getStatusCode());
//        assertThat(status).isEqualTo(Status.SUCCESS);
//        assertThat(response.getStatusCode()).isEqualTo(200);
//    }
//
//    @Ignore
////    @Test
////    void testParsing() throws IOException {
////        ObjectMapper mapper = new ObjectMapper();
////        TestObject testObject = mapper.readValue(Paths.get("src/test/resources/test.json").toFile(), TestObject.class);
////        assertThat(testObject.getBody()).isEqualToIgnoringCase("body");
//
//
//    }
//    @Ignore
//    @Test
//    void testDelete_Success(){
//        Response response = given()
//                .when()
//                .delete("/delete");
//        response.prettyPrint();
//        String contentType = response.getContentType();
//        assertThat(contentType).isEqualToIgnoringCase(ContentType.JSON.toString());
//        assertThat(response.getStatusCode()).isEqualTo(201);
//    }
//

//
//    @Test
//    void  testAuth_NoHeader_Forbidden(){
//        Response response = given()
//                .when()
//                .get("/bearer");
//        response.prettyPrint();
//        assertThat(response.getStatusCode()).isEqualTo(401);
//    }

}
